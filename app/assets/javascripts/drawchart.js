function drawsentichart()
{
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(renderchart);
      function renderchart() {
	var apple = ['Apple',  10,      100];
        var data = google.visualization.arrayToDataTable([
          ['Object', 'Positive', 'Negative'],
          apple,
          ['Google',  25,      15],
          
        ]);
        var options = {
          title: 'Sentiment_analysis',
          hAxis: {title: 'Year', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('sentigraph'));
        chart.draw(data, options);
      }
}

function drawpopularitychart()
{
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Apple', 'Google'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Popularity over time'
        };

        var chart = new google.visualization.LineChart(document.getElementById('somechart'));
        chart.draw(data, options);
      }
}