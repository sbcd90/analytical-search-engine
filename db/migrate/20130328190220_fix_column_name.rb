class FixColumnName < ActiveRecord::Migration
  def self.up
  	rename_column :searchings, :type, :grammar
  end

  def self.down
  end
end
